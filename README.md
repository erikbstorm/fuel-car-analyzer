# fuel-car-Analyzer

This program is made to analyze car data and fuel price data. In this case it is used to determine whether fuel efficient cars are bought
more frequently when fuel prices are higher. This project contains multiple programs written in Python 3.6.7 and bash.


##The Data

The data that you will need can be found on the RDW website and the rijks overheid data website.

###Car data: 
A .csv file with all registration plates and according details about the vehicle.
[link to car data file](https://opendata.rdw.nl/Voertuigen/Open-Data-RDW-Gekentekende_voertuigen/m9d7-ebf2) **7.6 GB**


###Fuel price data: 
A .json file with the date and according fuel prices.
[link to fuel data file](https://data.overheid.nl/dataset/53411-pompprijzen-motorbrandstoffen--brandstofsoort--per-dag) **~500kB**

##clean_data.sh
This program cleans the car data (.csv) file from excess information and leaves you with
only the registration plate number, energy efficiency label ,date when first bought 

###Usage
You can call the file using
```
./clean_data.sh (filename)
```
**BE AWARE** 
clean_data.sh will export the new file to *CLEANED_OUT_CAR_DATA.csv* and will overwrite if there is already a file named that.


##examine_data.py
This Python program will examine the filtered data and combine it with the fuel price data file.

###Usage
When finished with the clean_data.sh you can then use that data in this program.


```
python3 examine_data.py [file.csv] [file.json]
```

And the output will be printed onto the screen.