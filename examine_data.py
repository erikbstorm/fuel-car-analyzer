#!/usr/bin/python3
import json
import pandas as pd
import sys


def main(csv_pathname, json_pathname):
    print("Importing .csv file...", file=sys.stderr)
    cars = importData(csv_pathname)
    cars = cars.sort_values('Aanschafdatum')
    cars = cars.drop(columns=['Type'])
    cars = cars.groupby(['Aanschafdatum']).sum()
    
    print("Importing .json file...", file=sys.stderr)
    price = getPrices(json_pathname)

    print("Merging datasets...", file=sys.stderr)
    joined = pd.merge(cars, price, on='Aanschafdatum', how='inner')
    
    print("Analyzing data...", file=sys.stderr)
    analyzeFrame(joined)
    print("FINISHED", file=sys.stderr)

def importData(name):
    '''Imports the car data'''
    cars = pd.read_csv(name, low_memory=False, index_col=0)
    return cars

def getPrices(json_pathname):
    '''
    Takes the file name as an argument and returns the dates with the average fuel prices in a dataframe
    '''
    with open(json_pathname, 'r') as file:
        y = json.load(file)

    fuels = y['value']
    prices = []
    for price_dict in fuels:
        prices += [{ 'Aanschafdatum' : price_dict['Perioden'], 'price': (price_dict['BenzineEuro95_1'] + price_dict['Diesel_2'] + price_dict['LPG_3']) / 3 }] 

    prices = pd.DataFrame(prices)
    prices['Aanschafdatum']=prices['Aanschafdatum'].astype(int)

    return prices

def analyzeFrame(dataframe):
    '''
    Takes in the process data as a dataframe and converts it in a contingency table
    Returns a dataframe
    '''

    price_line = 1.14  # The price from which the fuel price is considered low and high
    efficient_cars = 'ABC' # The categories that fall in efficient cars
    
    output = {"efficient and low prices" : 0, "efficient and high prices" : 0,
                  "not efficient and low prices" : 0, "not efficient and high prices" : 0}

    for index, row in dataframe.iterrows():
        price = row['price']
        zk = row['Zuinigheidsklasse']
        
        if price <= price_line:
            for label in zk:
                if label in efficient_cars:
                    output['efficient and low prices'] += 1
                else:
                    output['not efficient and low prices'] += 1
        else:
            for label in zk:
                if label in efficient_cars:
                    output['efficient and high prices'] += 1
                else:
                    output['not efficient and high prices'] += 1
    
    for value in output:
        print(value, " : ", output[value])
    
    return output


if __name__ == '__main__':
    if len(sys.argv) == 3:
        main(sys.argv[1], sys.argv[2])
    else:
        print("Format: examine_data.py [file.csv] [file.json]", file=sys.stderr)