#!/bin/bash
#Use the first argument as the file you use for the data
#You must first give authorization to run this program with `chmod ogu+x clean_data.sh`
#EXCUSIVELY use data from the RDW with the specific format otherwise the program wont work
OUTFILENAME='CLEANED_OUT_CAR_DATA.csv'


OUTPUT=`cat $1 | cut -d',' -f1,2,20,22 | egrep -e 'Personenauto' -e 'Bedrijfsauto'`

echo "Kenteken,Type,Zuinigheidsklasse,Aanschafdatum" > $OUTFILENAME
echo $OUTPUT | tr ' ' '\n' | egrep '.+,.+,[ABCDEFG],[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' >> $OUTFILENAME